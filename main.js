const companies= [
    {name: "Company One", category: "Finance", start: 1981, end: 2004},
    {name: "Company Two", category: "Retail", start: 1992, end: 2008},
    {name: "Company Three", category: "Auto", start: 1999, end: 2007},
    {name: "Company Four", category: "Retail", start: 1989, end: 2010},
    {name: "Company Five", category: "Technology", start: 2009, end: 2014},
    {name: "Company Six", category: "Finance", start: 1987, end: 2010},
    {name: "Company Seven", category: "Auto", start: 1986, end: 1996},
    {name: "Company Eight", category: "Technology", start: 2011, end: 2016},
    {name: "Company Nine", category: "Retail", start: 1981, end: 1989}
  ];
  
  const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];


// FOR Loop
//   for(let i=0; i <= companies.length; i++){
//       console.log(companies[i]);
//   }


//FOREACH ES5
// companies.forEach(function(company){
//     console.log(company);
// });



//FILTER
/*
* ES5 Version
*/
// const canDrink = ages.filter(function(age){
//     if(age >= 21){
//         return true;
//     }
// });

/*
* ES6 Version
*/
// const canDrink = ages.filter(age => age >= 21);

// console.log(canDrink);

// const retailCompanies = companies.filter(company => company.category === 'Retail');
// console.log(retailCompanies);

//GET 80's Company
// const eightiesCompany = companies.filter(company => (company.start >= 1980 && company.start <= 1989) );
// console.log(eightiesCompany);

//Get Companies Lasted 10 year
// const companiesLastedTenYears = companies.filter(company => (company.end - company.start >= 10));
// console.log(companiesLastedTenYears);

//SORT
// /*
// * ES5 Version
// */
// const companiesSortByStartDate = companies.sort(function(c1, c2){
//     if(c1.start > c2.start){
//         return 1;
//     }else{
//         return -1;
//     }
// });

// console.log(companiesSortByStartDate);

/*
* ES6 Version
*/
// const companiesSortByStartDate = companies.sort((a,b) =>(a.start > b.start ? 1 : -1));

// console.log(companiesSortByStartDate);

// const sortAges = ages.sort((a, b) => (a > b ? 1: -1));
// console.log(sortAges);

// const sortAges = ages.sort(); //sorts only the first Digit
// const sortAges = ages.sort((a, b) => a - b); // Sorts on Descending Order
// const sortAges = ages.sort((a, b) => b - a); // Sorts on Ascending Order

const totalYears = companies.reduce((total, company) => total + (company.end - company.start), 0);
console.log(totalYears);